//通过改变入口和生成文件的大小来解决，该方法：可行。
module.exports = {
    configureWebpack : {
        //关闭webpack的性能提示
        // performance : {
        //     hints : false
        // },
        performance: {
            hints: 'warning',
            // 入口起点的最大体积
            maxEntrypointSize: 50000000,
            // 生成文件的最大体积
            maxAssetSize: 30000000,
            // 只给出 js 文件的性能提示
            assetFilter: function(assetFilename) {
                return assetFilename.endsWith('.js') || assetFilename.endsWith('.css');
            }
        },
    },
}