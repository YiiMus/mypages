export default {
    /**
     * 图片加载完成后淡入展示
     *
     * @param imgEle img元素
     */
    show(imgEle){
        //默认元素隐藏
        imgEle.style.opacity = "0";
        //设置动画时长
        imgEle.style.transition = "opacity 0.3s ease-in-out";
        imgEle.onload = function (){
            //元素显示
            imgEle.style.opacity = "1";
        }
    }
}