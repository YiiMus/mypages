export default {
    /**
     * 背景图加载完后显示
     *
     * @param el 需要加背景图的元素
     * @param url 图片地址
     */
    show(el,url){
        //创建image对象
        let image = new Image();
        //绑定上要加载的图片
        image.src = url;
        //当图片加载完后会调用onload方法
        image.onload = function (){
            if (el){
                //添加背景图
                el.style.backgroundImage = "url('"+image.src+"')";
            }
        }
    }
}