//更改网站图标
function changeFavicon(src) {
    let link = document.createElement('link'),
        oldLink = document.getElementById('icon');
    link.id = 'icon';
    link.rel = 'icon';
    link.href = src;
    if (oldLink) {
        document.head.removeChild(oldLink);
    }
    document.head.appendChild(link);
}

//更改网站标题
function changeTitle(title){
    document.title = title
}

//显示滚动条
function showScrollbar() {
    let oldLink = document.getElementById('scrollbar');
    if (oldLink) {
        document.head.removeChild(oldLink);
    }
}

export default {
    changeFavicon,
    changeTitle,
    showScrollbar
}
