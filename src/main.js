import { createApp } from 'vue'
import App from './App.vue'
import router from "./router/index";
//引入第三方动画库
import 'animate.css'
//使用vuex
import vuex from "./store/index"

createApp(App)
    .use(router)
    .use(vuex)
    .mount('#app')
