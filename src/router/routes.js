import Home from "../views/Home";
import About from "../views/About";
import Works from "@/views/Works";
import Snet from "@/components/works/work/Snet";

export default [
    {
        path: '/',
        component: Home,
        meta: {
            title: '引木u | 别来无恙',
            //是否显示顶栏
            isShowTopBar: true
        }
    },
    {
        path: '/about',
        component: About,
        meta: {
            title: '引木u | 关于',
            isShowTopBar: true
        }
    },
    {
        path: '/works',
        component: Works,
        meta: {
            title: '引木u | 作品',
            isShowTopBar: true
        }
    },
    {
        path: '/campus_network_tool',
        component: Snet,
        meta: {
            title: 'campus_network_tool | 校园网自动化认证工具',
            isShowTopBar: false
        }
    },
    //404匹配，跳转至主页
    {
        path: '/:pathMatch(.*)',
        redirect: '/',
    }
]