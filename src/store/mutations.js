const mutations = {
    changeShowTopBar(state){
        state.isShowTopBar = !state.isShowTopBar
    }
}

export { mutations }